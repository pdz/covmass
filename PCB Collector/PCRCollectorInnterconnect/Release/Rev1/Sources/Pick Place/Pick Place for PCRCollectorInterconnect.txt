Altium Designer Pick and Place Locations
C:\Users\dklockar\AppData\Local\TempReleases\Snapshot\1\Sources\Pick Place\Pick Place for PCRCollectorInterconnect.txt

========================================================================================================================
File Design Information:

Date:       11/05/21
Time:       01:09
Revision:   Not in VersionControl
Variant:    No variations
Units used: mil

Designator Comment                               Layer    Footprint           Center-X(mil) Center-Y(mil) Rotation Description                                                   
MH2        "Mounting Hole Plated 3.5mm Ring 7mm" TopLayer MHP3.5mm            4000.000      1900.000      0        "Mounting Hole Plated 3.5mm Ring 7mm"                         
MH1        "Mounting Hole Plated 3.5mm Ring 7mm" TopLayer MHP3.5mm            4000.000      200.000       0        "Mounting Hole Plated 3.5mm Ring 7mm"                         
R4         Res2                                  TopLayer AXIAL-0.4           5000.000      400.000       270      Resistor                                                      
R3         Res2                                  TopLayer AXIAL-0.4           4600.000      300.000       180      Resistor                                                      
R2         Res2                                  TopLayer AXIAL-0.4           650.000       900.000       180      Resistor                                                      
R1         Res2                                  TopLayer AXIAL-0.4           650.000       450.000       180      Resistor                                                      
Q1         NMOS-2                                TopLayer TO-220AB            5300.000      400.000       90       "N-Channel Power MOSFET"                                      
P12        "Header 4"                            TopLayer Molex171856-0004    4350.000      1425.000      180      "Header, 4-Pin"                                               
P11        691311400102                          TopLayer 691311400102        5717.717      400.000       90       "Serie 3114 - 7.62mm Close Vertical PCB Header WR-TBL, 2 pin" 
P10        "Header 8"                            TopLayer HDR1X8              4350.000      1200.000      360      "Header, 8-Pin"                                               
P9         "Header 8"                            TopLayer HDR1X8              4350.000      700.000       360      "Header, 8-Pin"                                               
P8         691311400102                          TopLayer 691311400102        5717.717      1450.000      90       "Serie 3114 - 7.62mm Close Vertical PCB Header WR-TBL, 2 pin" 
P7         "Header 4"                            TopLayer Molex171856-0004    2950.000      1425.000      180      "Header, 4-Pin"                                               
P6         "Header 8"                            TopLayer HDR1X8              2950.000      1200.000      360      "Header, 8-Pin"                                               
P5         "Header 8"                            TopLayer HDR1X8              2950.000      700.000       360      "Header, 8-Pin"                                               
P4         "Header 4"                            TopLayer Molex171856-0004    1650.000      1425.000      180      "Header, 4-Pin"                                               
P3         "Header 8"                            TopLayer HDR1X8              1650.000      1200.000      360      "Header, 8-Pin"                                               
P2         "Header 8"                            TopLayer HDR1X8              1650.000      700.000       360      "Header, 8-Pin"                                               
P1         "Header 20X2"                         TopLayer HDR2X20             150.000       1058.000      90       "Header, 20-Pin, Dual row"                                    
C4         22uF                                  TopLayer FP-EEU-FR1H101B-MFG 3731.102      1350.000      360      "CAP ALUM 100UF 20% 50V RADIAL"                               
C3         100uF                                 TopLayer FP-EEU-FR1H101B-MFG 5200.000      1450.000      90       "CAP ALUM 100UF 20% 50V RADIAL"                               
C2         22uF                                  TopLayer FP-EEU-FR1H101B-MFG 2350.000      1350.000      360      "CAP ALUM 100UF 20% 50V RADIAL"                               
C1         22uF                                  TopLayer FP-EEU-FR1H101B-MFG 1031.102      1350.000      360      "CAP ALUM 100UF 20% 50V RADIAL"                               
