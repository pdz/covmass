# ------------------
# create markers to display recognised circles on images
# ------------------

def markings(circles,output,style):
    # Import packages and functions
    # ------------------
    import cv2
    import numpy as np

    # Execution
    # ------------------
    # convert the (x, y) coordinates and radius of the circles to integers

    if style==2:
        # loop over the (x, y) coordinates and radius of the circles
        for (x, y, r) in circles:
            # draw circles and rectangle for center
            cv2.circle(output, (x, y), r, (255, 0, 0), 2)
            cv2.rectangle(output, (x - 5, y - 5), (x + 5, y + 5), (0, 0, 255), -1)

    if style==0:
        # loop over the (x, y) coordinates and radius of the circles
        for (x, y, r) in circles:
            # draw circles and rectangle for center
            cv2.circle(output, (x, y), r, (0, 255, 0), 2)
            cv2.rectangle(output, (x - 5, y - 5), (x + 5, y + 5), (0, 255, 0), -1)

    if style==1:
        # loop over the (x, y) coordinates and radius of the circles
        for (x, y, r) in circles:
            # draw circles and rectangle for center
            cv2.circle(output, (x, y), r, (0, 0, 255), 2)
            cv2.rectangle(output, (x - 5, y - 5), (x + 5, y + 5), (0, 0, 255), -1)

    return