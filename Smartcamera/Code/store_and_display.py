# ------------------
# Function to show and store images coming from the recognition
# ------------------

def store_and_display(mask, output, iteration_counter, circles_tubes):
    # Import packages and functions
    # ------------------
    from markings import markings

    import cv2

    # Execution
    # -------------------

    # unedited image
    # cv2.imshow("01 | unedited image", img)

    # Convert grayscale image to 3-channel, get markers and resize images so they can be stacked together
    mask = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)
    markings(circles_tubes, output, 1)
    img_combined = cv2.vconcat([mask, output])
    # img_combined = cv2.resize(img_combined, (int(img_combined.shape[1] * 0.7), int(img_combined.shape[0] * 0.7)))

    # Store combined images
    path = "/home/pi/Desktop/testing/" + str(iteration_counter) + "_tubes_detected.jpg"
    cv2.imwrite(path, img_combined)

    #  Show combined imags
    cv2.imshow('02 | Combined images', img_combined)
    cv2.waitKey(500)

    return
