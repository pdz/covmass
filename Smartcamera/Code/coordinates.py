# ------------------
# Processes and analyses images to find the tubes returned to the collector.
# ------------------

def coordinates(img, green_low, green_high, output_window, iteration_counter, rack_size):
    # Import packages and functions
    # ------------------
    from store_and_display import store_and_display
    from placement import placement

    import cv2
    import numpy as np

    # Execution
    # ------------------
    # Process raw image
    img_blur = cv2.medianBlur(img, 11)
    img_hsv = cv2.cvtColor(img_blur, cv2.COLOR_BGR2HSV)
    output = img.copy()

    # find tubes
    mask = cv2.inRange(img_hsv, green_low, green_high)
    circles_tubes = cv2.HoughCircles(mask, cv2.HOUGH_GRADIENT, 1, 80, param1=20, param2=8, minRadius=25, maxRadius=44)

    # handle results
    if circles_tubes is not None:

        # round and avoid radii that are not needed for further processing
        circles_tubes = np.round(circles_tubes[0, :]).astype("int")
        coordinates_tubes = circles_tubes[:, (0, 1)]

    else:
        coordinates_tubes = None

    positions = placement(coordinates_tubes, output_window, rack_size, False)

    # show and store images
    store_and_display(mask, output, iteration_counter, circles_tubes)

    return positions
