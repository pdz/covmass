# ------------------
# Processes and analyses image to find dots of different color. Especially the ones identifying a rack
# ------------------

def identification(img, red_low, red_high, output_window, rack_size):
    # Import packages and functions
    # ------------------
    from placement import placement
    from store_and_display import store_and_display

    import numpy as np
    import cv2

    # Function definition
    # -------------------
    # detemine rack number from marked holes
    def rack_identification(positions):

        result = 0
        if 14 in positions:
            result += 1
        if 22 in positions:
            result += 2
        if 13 in positions:
            result += 4
        if 21 in positions:
            result += 8
        if 12 in positions:
            result += 16
        if 20 in positions:
            result += 32
        if 19 in positions:
            result += 64

        return result

    # Execution
    # -------------------
    # process raw image
    img_blur = cv2.medianBlur(img, 11)
    img_hsv = cv2.cvtColor(img_blur, cv2.COLOR_BGR2HSV)
    output = img.copy()

    # find marked holes
    mask = cv2.inRange(img_hsv, red_low, red_high)
    circles_tubes = cv2.HoughCircles(mask, cv2.HOUGH_GRADIENT, 1, 65, param1=20, param2=8, minRadius=17, maxRadius=40)

    # handle results
    if circles_tubes is not None:

        # round and avoid radii that are not needed for further processing
        circles_tubes = np.round(circles_tubes[0, :]).astype("int")
        coordinates_tubes = circles_tubes[:, (0, 1)]

        # call the function to get hole numbers
        positions = placement(coordinates_tubes, output_window, rack_size, True)
        rack_number = rack_identification(positions)

        # show and store images
        store_and_display(mask, output, 0, circles_tubes)



    return positions, rack_number