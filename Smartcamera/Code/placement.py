# ------------------
# Find the hole a certain identified circle corresponds to. Smallest distance is relevant.
# ------------------

def placement(result, output_window, rack_size, identification):
    # Import packages and functions
    # ------------------
    import cv2
    import numpy as np

    # Function definition
    # -------------------
    def distance(result, holes):
        import numpy as np

        tube_count = result.shape[0]
        positions = np.zeros((tube_count, 1), dtype=np.int8)
        for i in range(0, tube_count):
            dist_2 = np.sum((holes - np.asarray(result[i, :])) ** 2, axis=1)
            positions[i, 0] = np.argmin(dist_2) + 1

        return positions

    # Execution
    # -------------------
    # calibration for grid to align with found tubes and compensate camera alignment
    calibrate_x = 0.2
    calibrate_y = -0.2
    calibrate_scale = 1.05

    if result is not None:

        # create blank window for visualisation of result
        blank_image = np.ones(shape=[output_window[0], output_window[1], 3], dtype=np.uint8) * 255

        # create grid for tube insertion
        spacing = calibrate_scale * 90
        holes = np.zeros((rack_size[0] * rack_size[1], 2))
        counter = 0
        for i in range(0, rack_size[0]):
            for j in range(0, rack_size[1]):
                holes[counter, 0] = output_window[1] / 2 - spacing * (
                        rack_size[1] / 2 - 0.5 - calibrate_x) + j * spacing
                holes[counter, 1] = output_window[0] / 2 - spacing * (
                        rack_size[0] / 2 - 0.5 - calibrate_y) + i * spacing
                counter = counter + 1

        # plot holes for tubes
        holes = np.round(holes).astype("int")
        for (x, y) in holes:
            cv2.circle(blank_image, (x, y), 26, (0, 0, 0), 2)

        # plot actual occupied holes
        dot_color = (0, 0, 255) if identification else (0, 255, 0)

        for (x, y) in result:
            cv2.circle(blank_image, (x, y), 10, dot_color, 20, cv2.FILLED)

        # plot combined solution on blank window
        cv2.imshow("04 | position in rack", blank_image)
        #cv2.waitKey(100)
        # cv2.imwrite('/home/pi/Desktop/image/positions.jpg', blank_image)

        # print positions in console
        positions = np.sort(distance(result, holes), 0)


    else:
        positions = np.array([[0]])

    return positions
