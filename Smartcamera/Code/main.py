# ---------------------------------------------------------------------------------------------
# Project: CovMass test tube recognition with OpenCV
# Version: 1.2
# Author : Stefan Villiger
# Github : stefanvilliger
#
# This script allows the recognition of the rack as well as the positions of returned tubes.
# The door can be controlled and closed by tracking the removal of the user's hand.
# A rack specific file with all users and respective positions can be exported.
# This file can then be used to export a .gwl file used for controlling the pipetting robot.

# Required calls:
# iterated_detection: For reliable image, processing and identification of the rack or the tubes. Specified by flag.
# hand_detection: To close the door safely after the hand is removed from the enclosure.
# log_positions: Save all the gathered information about rack and tubes to a .txt file
# export_gwl: Can be used to directly export .gwl files used by the pipetting robot to specify the movements.

# ---------------------------------------------------------------------------------------------

# Import packages and functions
# ------------------
from hand_detection import hand_detection
from log_positions import log_positions
from gwl_export import gwl_export
from iterated_detection import iterated_detection

import numpy as np

# define functions used in this script
# ------------------
# print evaluated positions
def print_positions(positions):
    print("The following", positions.shape[0], "slots are now occupied:", end=" ")
    for i in range(0, positions.shape[0]):
        print(positions[i, 0], end=", ", flush=True)
    print("\n")
    return


# find placement and number of newly added tubes
def find_change(positions, positions_old):
    positions_change = [value for value in positions if value not in positions_old]
    if len(positions_change) == 1:
        print("corect addition of only one tube")
    else:
        print("CAUTION: added tubes differ from one")
    print("\n")
    return positions_change


# Specify flags and variables for enabling functions in the software
# -------------------
identification_flag = True
iterations = 10

# Execution
# ------------------
# initialise variables
positions_old = np.array([[0]])

# Identify Rack or mark as 0
rack = iterated_detection(True, 0) if identification_flag else 0

# run tube detection for specified number of time
for iteration_counter in range(1, iterations + 1):
    try:
        # call the iterated_detection function
        print("Outputs for following iteration: ", iteration_counter)

        # wait until hand is detected and removed again
        hand_detection()

        # find and print positions
        positions = iterated_detection(False, iteration_counter)
        print_positions(positions)

        # find and print change of tubes
        positions_change = find_change(positions, positions_old)

        # log positions in .txt file
        log_positions(rack, positions, iteration_counter, positions_change)

        # update for storage of old positions for upcoming comparison
        positions_old = positions
    except:
        print("ERROR: error with the call!! positions couldn't be identified and written to .txt file!!")

# export .gwl file for robot after everything is finished
gwl_export(positions)
