# ------------------
# Calculation of distances from tubes to holes.
# ------------------

def distance(result, holes):
    # Import packages and functions
    # ------------------
    import numpy as np

    # Execution
    # ------------------
    tube_count = result.shape[0]
    positions = np.zeros((tube_count, 1), dtype=np.int8)
    for i in range(0, tube_count):
        dist_2 = np.sum((holes - np.asarray(result[i, :])) ** 2, axis=1)
        positions[i, 0] = np.argmin(dist_2) + 1

    return positions
