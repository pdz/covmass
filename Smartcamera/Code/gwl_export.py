# ------------------
# Combine probes to pools and export .gwl file for pipetting robot.
# ------------------

def gwl_export(positions):
    # Import packages and functions
    # ------------------
    from dataclasses import dataclass

    # Function definition
    # -------------------
    # combine probes to pools
    def pooling_position(samples, hole_nr):
        # Very ugly function for the assignment of probes to their pool.
        # Works for given sizes of 4 and 8, not tested for others and probably wrong results!
        # TODO: write cleaner and more flexible function

        pool_min = 4
        pool_max = 8
        complete_pools = None
        halved_pools = None
        in_remaining_pool = None

        complete_pools = samples // pool_max
        remaining = samples % pool_max

        # Compute possible configuration for pooling
        # Check if more than the pool_min is available
        if samples >= 4:
            # check if one pool needs to be distributed to add half of it to the remaining samples so they are > 4
            if remaining < pool_min and remaining != 0:
                complete_pools = complete_pools - 1
                halved_pools = 1
                in_remaining_pool = remaining + 4
                computed = complete_pools * 8 + halved_pools * 4 + in_remaining_pool
            # do it directly if remaining is > 4
            else:
                complete_pools = complete_pools
                computed = complete_pools * 8 + remaining
        else:
            # if samples < 4
            too_few_samples = True

        # assign to pools considering calculated sizes before
        if ((hole_nr - 1) // pool_max) < complete_pools:
            pooling_tube = (hole_nr - 1) // pool_max + 1
        elif halved_pools != None:
            if (hole_nr - complete_pools * pool_max - halved_pools * pool_min) > 0:
                pooling_tube = complete_pools + 2
            else:
                pooling_tube = complete_pools + 1
        else:
            pooling_tube = complete_pools + 1

        return pooling_tube

    # function definition to combine information into one line per operation
    def write_line(rack_type, position):
        line = rack_type.operation + ";" + rack_type.label + ";;" + rack_type.type + ";" + str(position) + ";;" + \
               rack_type.volume + ";;;;" + rack_type.type + "\n"
        return line

    # variables and datasets
    # ------------------
    # variable to invert if not enough samples for pooling
    too_few_samples = False

    # define datatype and racks for it
    @dataclass
    class rack:
        operation: str
        label: str
        type: str
        volume: str

    CovMass_rack = rack("A", "Rack 1", "CoVMass 50mL TubeRack", "1000")
    Storage_rack = rack("D", "96 DeepWell", "CoVMass 96 DeepWell", "900")
    Pooling_rack = rack("D", "Cobas", "CovMas 12Pos Cobas Rack", "100")

    # find number of samples
    samples = positions.shape[0]

    # Execution
    # ------------------
    # open .gwl file to write to
    f = open("rack_1.gwl", "w+")

    counter = 1
    for hole_nr in positions:
        # get text lines for following operations and current hole_nr
        pooling_tube = pooling_position(samples, counter)

        aspirate = write_line(CovMass_rack, int(hole_nr))
        dispense_storage = write_line(Storage_rack, counter)
        dispense_pooling = write_line(Pooling_rack, pooling_tube)

        f.write(aspirate)
        f.write(dispense_storage)
        f.write(dispense_pooling)
        f.write("W;\n")

        counter += 1

    if too_few_samples:
        print("ERROR: not enough samples!")
    return
