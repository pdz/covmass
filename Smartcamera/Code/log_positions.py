# ------------------
# Log positions and write them to .txt file
# ------------------

def log_positions(rack, positions, iteration_counter, positions_change):
    # Execution
    # ------------------
    if iteration_counter == 1:
        # create .txt file for staroge of information
        f = open("positions.txt", "w+")
        text = "storage of positions in rack no. " + str(rack) + " for each iteration \n\n"
        f.write(text)
    f = open("positions.txt", "a")
    text = "Iteration: " + str(iteration_counter) + "\tNumber of tubes  added: " + str(
        len(positions_change)) + "\tThe following " + str(
        positions.shape[0]) + " slots are occupied: "
    for i in range(0, positions.shape[0]):
        text = text + str(positions[i, 0]) + ", "

    text = text + "\n"
    f.write(text)
    return
