# ------------------
# Function to wait until hand was inserted and removed again
# Difference in brightness between HSV images proofed to be very reliable. Mask is applied over threshold.
# Pixels can be counted to realise threshold after tube is placed and hand removed. (Tube equals ca. 3200px)
# Fixing brightness is not enough -> is calculated over hole image. Fixing ISO and shutter is most reliable.
# CAUTION: Movement of the camera and jitters will also affect the pixel change measurement!
#
# Different things were also tested:
# Deep learning and google mediapipe -> slow, bad recognition toward edges
# SSIM -> conversion to gray not beneficial due to similar brightness, not satisfactory
# ------------------

def hand_detection():
    # Import packages and functions
    # ------------------
    from picamera.array import PiRGBArray
    from picamera import PiCamera
    import time
    import cv2

    # Function definition
    # -------------------
    def grab_image(camera):
        camera.resolution = (800, 608)  # Raspi cam:2592 x 1944 -> 4x3 with multiple of 16px to avoid resizing
        camera.shutter_speed = 10000
        camera.ISO = 800
        rawCapture = PiRGBArray(camera)
        camera.capture(rawCapture, format="bgr")
        image = rawCapture.array  # actual image is taken
        image_hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)  # conversion to HSV
        return image, image_hsv

    # Variable definition
    # -------------------
    hand = False
    threshold = 4000

    # Execution
    # -------------------
    # take first image
    camera = PiCamera()
    time.sleep(1)  # some time for the camera to initialise before capturing the image
    before, before_hsv = grab_image(camera)
    # visualisation for first image
    # cv2.imshow("before", before)
    # cv2.waitKey(1000)

    # loop for pictures to compare
    for x in range(1, 10000):
        # grab following image
        after, after_hsv = grab_image(camera)

        # visualisation of image
        # cv2.imshow("after", after)

        # comparing images
        result_hsv = cv2.absdiff(before_hsv, after_hsv)
        result = cv2.cvtColor(result_hsv, cv2.COLOR_HSV2BGR)
        result2 = cv2.inRange(result_hsv, (0, 0, 100), (179, 255, 255))
        pixels = cv2.countNonZero(result2)

        # print(pixels)
        # cv2.imshow("result", result)
        # cv2.imshow("result2", result2)
        # cv2.waitKey(100)

        if pixels > threshold and hand == False:
            print("Hand inserted!")
            hand = True
        if hand == True and pixels < threshold:
            print("Hand removed!")
            camera.close()
            return


    camera.close()
    return
