# ---------------------------------------------------------------------------------------------
# Iterate over multiple analysed images to compare results and find errors.
# Multiple runs of possible comparisons to provide tolerance for random errors in images
# ---------------------------------------------------------------------------------------------

def iterated_detection(identification_flag, iteration_counter):
    # Import packages and functions
    # -------------------
    from picamera.array import PiRGBArray
    from picamera import PiCamera
    import time
    import cv2

    from coordinates import coordinates
    from identification import identification

    # Function definition
    # -------------------
    # Take an image
    def grab_image():
        camera = PiCamera()
        camera.resolution = (800, 608)  # Raspi cam:2592 x 1944 -> 4x3 with multiple of 16px to avoid resizing
        rawCapture = PiRGBArray(camera)
        time.sleep(1)  # some time for the camera to initialise before capturing the image
        camera.capture(rawCapture, format="bgr")
        camera.close()
        img = rawCapture.array
        img = img[120:550, 0:800]
        return img

    # Variable definition
    # -------------------

    # low and high limit for colour detection | https://www.rapidtables.com/convert/color/rgb-to-hsv.html
    red_low = (160, 50, 100)
    red_high = (180, 255, 255)
    green_low = (30, 10, 150)
    green_high = (80, 255, 255)

    # size of output window for rack
    output_window = (430, 800)
    rack_size = (4, 8)

    # define variables to specify safety and reliability features
    compared_per_round = 2  # images to compare to secure reliable tracking and avoid faulty images
    additional_rounds = 1  # additional chances if error occured in first round

    # Execution
    # ------------------
    # Run identification multiple times in loops
    # additional_rounds:    allows for additional rounds if faulty images occured in first round
    # compared_per_round:   compares multiple iterations of detected points to ensure reliable detection
    comparison_error = False  # initialise state
    for i in range(1, additional_rounds + 2):
        for j in range(1, compared_per_round + 1):

            # grab new image to evaluate in every new attempt
            img = grab_image()
            # cv2.imshow("1 | raw", img)
            cv2.imwrite('/home/pi/Desktop/image/img_raw.jpg', img)

            # find rack number if flag for that is set
            if identification_flag:
                try:
                    positions, rack_number = identification(img, red_low, red_high, output_window, rack_size)
                except:
                    print("ERROR: Positions for identification couldn't be identified and printed")

            # call function to determine placement of tubes relative to hole grid
            else:
                # find locations of tubes in the rack
                try:
                    # call functions to identify tubes and find their holes
                    positions = coordinates(img, green_low, green_high, output_window, iteration_counter, rack_size)

                except:
                    comparison_error = True
                    print("ERROR: Positions couldn't be identified and printed")
                    break

            # compare results after first try when there is something to compare to
            try:
                if j > 1:
                    if positions is None and iteration_counter == 1:
                        print("SUCCESS: positions are NONE!")
                    elif list(positions_old) == list(positions):
                        print("SUCCESS: positions are equal!")
                    else:
                        comparison_error = True
                        print("ERROR: positions are different or nothing recognised!")
                        break
                else:
                    print("SUCCESS: first detection completed!")

                # store positions to compare
                positions_old = positions

            except:
                comparison_error = True
                print("ERROR: no comparison possible")

        # Everything successfull and points detected
        # Evaluate what to return to function call upon "identification_flag"
        if comparison_error == False:
            print("Points reliably detected!")
            if identification_flag:
                print("The following rack was detected: ", rack_number, "\n")
                return rack_number
            else:
                return positions
        else:
            comparison_error = False
            print("Run additional comparison in 3 seconds!")
            time.sleep(3)

    print("ERROR: Multiple faulty recognitions and rack should be inspected")
