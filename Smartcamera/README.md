# Adding Computer Vision to the CovMass Collector

How Computer Vision can be efficiently applied for system integration using a RaspberryPi and OpenCV. 
The multifunctional upgrade of a CovMass collector using a single image sensor to combine different manual steps and machines into one continuous workflow.

This System was developed as a sesemter thesis at ETH Zurich. 
It detects tubes used for sample deposition in a Covid test machine. 
Their positions are processed and used to simplify further processing at the lab and automatically export .gwl files that are used to control the pipetting robot handling the fluid.
Hand detection was added to simplify timing and safety of the door operation in the collector.

This file contains the documentation of all the material and steps that were used and are required to setup and run the code.
## Hardware
List of material used to reproduce the system. 
### Electronics
All electronic and hardware components used for the setup.
- Raspberry Pi 4 8GB
- Raspberry Pi camera V2.1
- Camera housing STL parts
- 4x M2 countersunk screws
- 30cm CSI cable
- USB C power adapter
### Markings
Different plotted stickers to mark additional points of interest for the detection.
- Reference Points for corners: Red, D=10mm
- Rack markers for holes: Red, Di=30mm, Do=40mm
## Software
Specific software versions were used to ensure the compatibility. 
Combination of Python and OpenCV versions is most critical.
- Python: 3.7.3
- OpenCV: 4.5.4.60

"testing_call.py" is the example for a possible function call. It calls all other scripts in the correct order.
## Environment
PyCharm professional was used to allow for SSH programming. 
Software was developed on the Mac and run on the Raspberry Pi. 
The Devices need to be in the same network.
1. Change  interpreter to "SSH interpreter"
2. Use login credentials for the Raspberry Pi
3. Check "execute code using this interpreter with root privileges via sudo"

Preinstalled VNC Software was used to create remote access and visibility to the Raspberry Pi.

1. Enable the VNC option in Raspberry Pi configuration
2. Install VNC Viewer on the Mac
3. Log in using credentials for the Raspberry Pi



