# CoVMass Dispenser Dokumentation


Welcome to the CovMass Documentation Git

If you would like to adapt the CoVMass project or are interested in the Hardware parts of the Project please feel free to explore this Git.


**Dispenser System:**
CoVMass partnered with Selecta to develop a reliable PCR-Saliva-Test dispenser. If you are interested in the Dispenser system please check out the document "Hardware Documentation". It describes not only the project but also contains a manual for the Dispenser modification.

**Materiallist**
If one of the component fails or needs to be replaced, use the "List of Materials" to reorder the individual components. It provids also list to supliers that have been used during the project


**Toollist for Maintenance**
Torx/Star screwdriver 		T10 and T15
slit screwdriver		S2.5 or S3
Cable Stripper			
Fresh cables 
electrical tape and/or shrinking tubes		


**Recent Updtes**
N/A

